package entity;

public enum ProductType {

    NATURAL_FLOWER,
    ARTIFICIAL_FLOWER,
    GIFT_CARD
}
