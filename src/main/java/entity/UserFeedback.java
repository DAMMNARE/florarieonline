package entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "user_feedback")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserFeedback {

    @Id
    private int id;

    @Enumerated(value = EnumType.STRING)
    private Rating rating;

    @Column(name = "feedback_description")
    private String feedbackDescription;

    @OneToOne
    private Client client;

    @Override
    public String toString() {
        return client.getFirstName() + " " + rating + ": " + feedbackDescription ;
    }
}
