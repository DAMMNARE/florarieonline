package config;

import entity.*;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class DatabaseConfig {

    private static SessionFactory sessionFactory = null;

    private DatabaseConfig() {
    }

    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            sessionFactory = new Configuration()
                    .configure("hibernate.cfg.xml")
                    .addAnnotatedClass(Client.class)
                    .addAnnotatedClass(Product.class)
                    .addAnnotatedClass(Supplier.class)
                    .addAnnotatedClass(Services.class)
                    .addAnnotatedClass(UserFeedback.class)
                    .buildSessionFactory();
        }
        return sessionFactory;
    }
}
