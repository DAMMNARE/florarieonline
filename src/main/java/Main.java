import config.DatabaseConfig;
import entity.*;
import org.hibernate.Session;
import org.hibernate.Transaction;
import repository.*;

import java.time.LocalDate;
import java.util.List;

public class Main {

    public static SupplierRepository supplierRepository =
            new SupplierRepository(DatabaseConfig.getSessionFactory());

    public static ClientRepository clientRepository =
            new ClientRepository(DatabaseConfig.getSessionFactory());

    public static ProductRepository productRepository =
            new ProductRepository(DatabaseConfig.getSessionFactory());

    public static ServicesRepository servicesRepository =
            new ServicesRepository(DatabaseConfig.getSessionFactory());

    public static UserFeedbackRepository userFeedback =
            new UserFeedbackRepository(DatabaseConfig.getSessionFactory());


    public static void main(String[] args) {

        Product rose = new Product(19, ProductType.NATURAL_FLOWER,
                "Rose", "Red Rose",
                100, 5.00, 10.00,
                LocalDate.now(), LocalDate.now().plusMonths(2));

        Product tulips = new Product(25, ProductType.NATURAL_FLOWER,
                "Tulip", "Tulip Holland",
                300, 4.00, 7.00,
                LocalDate.now(), LocalDate.now().plusMonths(3));

        productRepository.save(rose);
        productRepository.save(tulips);

        Client ion =  new Client(10, "Ion", "Pop", "ion.pop@gmail.com");
        Client maria = new Client(20, "Maria", "Murmurer", "maria.mureșan@gmail.com", List.of(rose, tulips));

        clientRepository.save(ion);
        clientRepository.save(maria);
        System.out.println(clientRepository.getAll());

        Supplier s1 = new Supplier(1, "KFC", "Bucharest - Romania");
        Supplier s2 = new Supplier(2, "Sunflower", "Oland");

        supplierRepository.save(s1);
        supplierRepository.save(s2);

        Session session1 = DatabaseConfig.getSessionFactory().openSession();
        Transaction t = session1.beginTransaction();

        Supplier s3 = new Supplier(3, "Margareta", "Poland");
        Client c1 = new Client(1, "Madalin", "Ionut", "madalin.ionut@yahoo.com");
        Client c2 = new Client(2, "Teodora", "Ecaterina", "teodora_teo@gmail.com");
        Client c3 = new Client(3, "George", "Andrei", "andrei_george@yahoo.com");

        session1.persist(s3);
        session1.persist(c1);
        session1.persist(c2);
        session1.persist(c3);

        t.commit();
        session1.close();

        Client c4 = new Client(4, "Margareta", "Alina", "alina.alina@yahoo.com");
        clientRepository.save(c4);

        Product p1 = new Product(1, ProductType.NATURAL_FLOWER, "Black Lilies", "A beautiful flower",
                10, 15.5, 25.7, LocalDate.of(2023, 2, 13),
                LocalDate.of(2023, 6, 28));

        productRepository.save(p1);

        displayAllClients();
        displayAllSupplier();
        displayAllProduct();

    }

    public static void displayAllClients() {
        List<Client> clients = clientRepository.getAll();
        for (Client c : clients) {
            System.out.println("First Name: " + c.getFirstName() + " Last Name: " + c.getLastName() + " Email: " + c.getEmail());
        }
    }

    public static void displayAllSupplier() {
        List<Supplier> suppliers = supplierRepository.getAll();
        for (Supplier s : suppliers) {
            System.out.println("Name Supplier: " + s.getSupplierName() + " Location: " + s.getLocation());
        }
    }

    public static void displayAllProduct() {
        List<Product> products = productRepository.getAll();
        for (Product p : products) {
            System.out.println("Name Product: " + p.getProductName() +
                    " Description product: " + p.getDescription() +
                    " Quantity: " + p.getQuantity() +
                    " Product type: " + p.getProductType() +
                    " Buying Date: " + p.getBuyingDate() +
                    " Expire Date: " + p.getExpiredDate() +
                    " Buying Price: " + p.getBuyingPrice() +
                    " Selling price: " + p.getSellingPrice());
        }
    }
}
