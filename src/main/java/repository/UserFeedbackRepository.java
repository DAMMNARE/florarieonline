package repository;

import entity.Rating;
import entity.UserFeedback;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

public class UserFeedbackRepository {

    private final SessionFactory sessionFactory;

    public UserFeedbackRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void save(UserFeedback userFeedback) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        session.persist(userFeedback);

        transaction.commit();
        session.close();
    }

    public List<UserFeedback> getAll() {
        List<UserFeedback> userFeedbacks;
        Session session = sessionFactory.openSession();

        userFeedbacks = session.createQuery("SELECT u FROM UserFeedback u", UserFeedback.class).getResultList();
        session.close();
        return userFeedbacks;
    }

    public void updateUserFeedback(UserFeedback userFeedback) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.merge(userFeedback);
        transaction.commit();
        session.close();
    }

    public void deleteUserFeedback(UserFeedback userFeedback) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.remove(userFeedback);
        transaction.commit();
        session.close();
    }

    public List<UserFeedback> getAllPositiveFeedbacks() {
        Session session = sessionFactory.openSession();
        List<UserFeedback> positiveFeedbacks = session.createQuery
                ("SELECT uf FROM UserFeedback WHERE uf.rating LIKE '" + Rating.GOOD.name() + "'", UserFeedback.class).getResultList();
        session.close();
        return positiveFeedbacks;
    }
}
