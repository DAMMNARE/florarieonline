package repository;

import entity.Supplier;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

public class SupplierRepository {

    private final SessionFactory sessionFactory;

    public SupplierRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void save(Supplier supplier) {
        Session session = sessionFactory.openSession();  // deschide o sesiune de comunicare cu baza de date
        Transaction transaction = session.beginTransaction(); // incepe o tranzactie

        session.persist(supplier);     // avem o singura modificare de facut si anume: salvarea supplier-ului

        transaction.commit(); /// salveaza in baza de date modificarile facute dupa transactie
        session.close(); // inchide sesiunea de comunicare cu baza de date.
    }

    public List<Supplier> getAll() {
        List<Supplier> suppliers;
        Session session = sessionFactory.openSession();

        suppliers = session.createQuery("SELECT s FROM Supplier s", Supplier.class).getResultList();
        session.close();
        return suppliers;
    }
}
